﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class MainClass
    {
        static void Main(string[] args)
        {
            double[,] firstMatrix = new double[,] { { 1, 3, 5, 7 }, { 9, 11, 13, 15 }, { 17, 19, 21, 23 }, { 25, 27, 29, 31 } };
            double[,] secondMatrix = new double[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
            double[,] thirdMatrix = new double[,] { { 1, 2, 3}, { 5, 6, 7}, { 9, 10, 11}, { 13, 14, 15} };
            int rows1 = 4;
            int columns1 = 4;
            int rows2 = 4;
            int columns2 = 4;
            int rows3 = 4;
            int columns3 = 3;
            MyMatrix matrix1 = new MyMatrix(firstMatrix, rows1, columns1);
            MyMatrix matrix2 = new MyMatrix(secondMatrix, rows2, columns2);
            MyMatrix matrix7 = new MyMatrix(thirdMatrix, rows3, columns3);
            MyMatrix matrix3 = matrix1 + matrix2;
            MyMatrix matrix4 = matrix1 - matrix2;
            MyMatrix matrix5 = -matrix1;
            MyMatrix matrix6 = matrix1 * matrix7;
            MyMatrix matrix8 = matrix1*2;
            Console.WriteLine("Матрица 1:\r\n");
            Console.WriteLine(matrix1);
            Console.WriteLine("Матрица 2:\r\n");
            Console.WriteLine(matrix2);
            if (matrix1 != matrix2)
                Console.WriteLine("Матрицы не равны\r\n");
            else
                Console.WriteLine("Матрицы равны\r\n");
            Console.WriteLine("Матрица 3:\r\n");
            Console.WriteLine(matrix7);
            Console.WriteLine("Отрицательная Матрица 1: \r\n");
            Console.WriteLine(matrix5);
            Console.WriteLine("Матрица 1 * 2: \r\n");
            Console.WriteLine(matrix8);
            Console.WriteLine("Транспонирование Матрицы 1: \r\n");
            Console.WriteLine(matrix7.Transpon());
            Console.WriteLine("Матрица 1 + Матрица 2\r\n");
            if (matrix3 == null)
            {
                Console.WriteLine("Нельзя складывать матрицы разных форматов!\r\n");
            }
            else
                Console.WriteLine(matrix3);
            Console.WriteLine("Матрица 1 - Матрица 2\r\n");
            if (matrix4 == null)
            {
                Console.WriteLine("Нельзя складывать матрицы разных форматов!\r\n");
            }
            else
                Console.WriteLine(matrix4);
            Console.WriteLine("Матрица 1 * Матрица 3\r\n");
            if (matrix6 == null)
            {
                Console.WriteLine("Нельзя перемножать эти матрицы!\r\n");
            }
            else
                Console.WriteLine(matrix6);
            Console.ReadKey();
        }
    }
}
