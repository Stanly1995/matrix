﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class MyMatrix
    {
        double[,] _matrix;
        int _rows;
        int _columns;
        public double[,] matrix
        {
            get { return _matrix; }
        }
        public int rows
        {
            get { return _rows; }
        }
        public int columns
        {
            get { return _columns; }
        }



        public MyMatrix(double[,] matrix, int rows, int columns)
        {
            _matrix = matrix;
            _rows = rows;
            _columns = columns;
        }

        public static MyMatrix operator +(MyMatrix a, MyMatrix b)
        {
            if (a.rows == b.rows && a.columns == b.columns)
            {
                MyMatrix c = new MyMatrix(new double[a.rows, a.columns], a.rows, a.columns);
                for (int i = 0; i < a.rows; i++)
                {
                    for (int j = 0; j < b.columns; j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] + b.matrix[i, j];
                    }
                }
                return c;
            }
            else
                return null;       
        }



        public static MyMatrix operator -(MyMatrix a, MyMatrix b)
        {
            if (a.rows == b.rows && a.columns == b.columns)
            {
                MyMatrix c = new MyMatrix(new double[a.rows, a.columns], a.rows, a.columns);
                for (int i = 0; i < a.rows; i++)
                {
                    for (int j = 0; j < b.columns; j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] - b.matrix[i, j];
                    }
                }
                return c;
            }
            else
                return null;

        }

        public static MyMatrix operator *(MyMatrix a, MyMatrix b)
        {
            if (a.columns == b.rows)
            {
                MyMatrix c = new MyMatrix(new double[a.rows, b.columns], a.rows, b.columns);
                for (int i = 0; i < a.rows; i++)
                {
                    for (int j = 0; j < b.columns; j++)
                    {
                        for (int k = 0; k<a.rows; k++)
                        {
                            c.matrix[i, j] += a.matrix[i, k]*b.matrix[k,j];
                        }

                    }
                }
                return c;
            }
            else
                return null;
        }

        public static MyMatrix operator *(MyMatrix a, float b)
        {

            MyMatrix c = new MyMatrix(new double[a.rows, a.columns], a.rows, a.columns);
            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.columns; j++)
                {
                    c.matrix[i, j] = a.matrix[i, j]*b;
                }
            }
            return c;
        }
        public static MyMatrix operator *(float b, MyMatrix a)
        {

            MyMatrix c = new MyMatrix(new double[a.rows, a.columns], a.rows, a.columns);
            for (int i = 0; i < a.rows; i++)
            {
                for (int j = 0; j < a.columns; j++)
                {
                    c.matrix[i, j] = a.matrix[i, j] * b;
                }
            }
            return c;
        }

        public static MyMatrix operator -(MyMatrix a)
        {

                MyMatrix c = new MyMatrix(new double[a.rows, a.columns], a.rows, a.columns);
                for (int i = 0; i < a.rows; i++)
                {
                    for (int j = 0; j < a.columns; j++)
                    {
                        c.matrix[i, j] = -a.matrix[i, j];
                    }
                }
                return c;
            }

        public MyMatrix Transpon()
        {
            MyMatrix c = new MyMatrix(new double[columns, rows], columns, rows);
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    c.matrix[j, i] = matrix[i, j];
                }
            }
            return c;
        }

        public static bool operator ==(MyMatrix a, MyMatrix b)
        {
            bool c = true;
            //if (a.rows == b.rows && a.columns == b.columns)
            //{
            //    for (int i = 0; i < a.rows; i++)
            //    {
            //        for (int j = 0; j < b.columns; j++)
            //        {
            //            if (a.matrix[i, j] != b.matrix[i, j])
            //            {
            //                c = false;
            //                break;
            //            }
            //        }
            //    }
            //}
            //else
            //    c = false;
            return c;
        }

        public static bool operator !=(MyMatrix a, MyMatrix b)
        {
            bool c = false;
            if (a.rows == b.rows && a.columns == b.columns)
            {
                for (int i = 0; i < a.rows; i++)
                {
                    for (int j = 0; j < b.columns; j++)
                    {
                        if (a.matrix[i, j] != b.matrix[i, j])
                        {
                            c = true;
                            break;
                        }
                    }
                }
            }
            else
                c = true;
            return c;
        }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            
            for (int i=0; i<rows; i++)
            {
                for (int j=0; j<columns; j++)
                {
                    str.Append(matrix[i, j] + "  ");
                }
                str.Append("\r\n");
                str.Append("\r\n");
            }
            return str.ToString();
        }
    }
}
